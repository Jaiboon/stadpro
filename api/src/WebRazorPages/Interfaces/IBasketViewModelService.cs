﻿using stadpro.RazorPages.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace stadpro.RazorPages.Interfaces
{
    public interface IBasketViewModelService
    {
        Task<BasketViewModel> GetOrCreateBasketForUser(string userName);
    }
}
