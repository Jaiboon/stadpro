using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [Route("[controller]/[action]")]
    public class AptitudeController : Controller
    {
        private readonly IAptitudeService _aptitudeService;

        public AptitudeController(IAptitudeService aptitudeService)
        {
            _aptitudeService = aptitudeService;
        }

        [HttpPost]
        public void AptitudeAdd([FromBody] Aptitude entity)
        {
            _aptitudeService.AddAptitude(entity);
        }

        [HttpPost]
        public async Task<Aptitude> Add([FromBody] Aptitude entity)
        {
            return await _aptitudeService.AddAsync(entity);
        }

        [HttpGet]
        public async Task<Aptitude> GetAptitudeById(int id)
        {
            return await _aptitudeService.GetByIdAsync(id);
        }

        [HttpGet]
        public async Task<IReadOnlyList<Aptitude>> ListAllAsync()
        {
            return await _aptitudeService.ListAllAsync();
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteAptitude(int id)
        {
            var resource = await _aptitudeService.GetByIdAsync(id);
            await _aptitudeService.DeleteAsync(resource);
            return Ok();
        }

        [HttpGet]
        public IList<Aptitude> GetAptitudeAsync(int num, string text, bool status, DateTime start_date)
       {
           var resource =  _aptitudeService.GetAptitudeAsync(num,  text,  status,  start_date);
           return resource;
       }
    }
}