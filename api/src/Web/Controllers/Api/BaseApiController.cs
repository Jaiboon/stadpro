﻿using stadpro.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace stadpro.Web.Controllers.Api
{
    [Route("api/[controller]/[action]")]
    public class BaseApiController : Controller
    { }
}
