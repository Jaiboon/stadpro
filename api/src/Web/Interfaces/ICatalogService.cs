﻿using Microsoft.AspNetCore.Mvc.Rendering;
using stadpro.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace stadpro.Web.Services
{
    public interface ICatalogService
    {
        Task<CatalogIndexViewModel> GetCatalogItems(int pageIndex, int itemsPage, int? brandId, int? typeId);
        Task<IEnumerable<SelectListItem>> GetBrands();
        Task<IEnumerable<SelectListItem>> GetTypes();
    }
}
