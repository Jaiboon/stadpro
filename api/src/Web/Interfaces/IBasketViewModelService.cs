﻿using stadpro.Web.ViewModels;
using System.Threading.Tasks;

namespace stadpro.Web.Interfaces
{
    public interface IBasketViewModelService
    {
        Task<BasketViewModel> GetOrCreateBasketForUser(string userName);
    }
}
