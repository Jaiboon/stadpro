﻿using stadpro.ApplicationCore.Entities.OrderAggregate;
using System.Threading.Tasks;

namespace stadpro.ApplicationCore.Interfaces
{
    public interface IOrderService
    {
        Task CreateOrderAsync(int basketId, Address shippingAddress);
    }
}
