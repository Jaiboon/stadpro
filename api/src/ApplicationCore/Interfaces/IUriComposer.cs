﻿namespace stadpro.ApplicationCore.Interfaces
{
    public interface IUriComposer
    {
        string ComposePicUri(string uriTemplate);
    }
}
