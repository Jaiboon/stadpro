using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces
{
    public interface ISPFolioRepository
    {
        // Get By Condition
        IList<Aptitude> GetAptitudeAsync(int num, string text, bool status, DateTime start_date);
        // save
        void SaveAptitude(Aptitude model);
        // delete
        void DeleteAptitude(Aptitude model);
    }
}