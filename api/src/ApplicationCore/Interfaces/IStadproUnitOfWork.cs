using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IStadproUnitOfWork
    {
        void SaveChanges();
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
        Task CompleteAsync();

    }
}