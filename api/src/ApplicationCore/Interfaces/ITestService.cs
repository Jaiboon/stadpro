using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using stadpro.ApplicationCore.Interfaces;

namespace ApplicationCore.Interfaces
{
    public interface ITestService
    {
        Task<Test> AddAsync(Test entity);
        Task<IReadOnlyList<Test>> ListAllAsync();
        Task<IReadOnlyList<Test>> ListAsync(ISpecification<Test> spec);
        Task<Test> GetByIdAsync(int id);
        Task UpdateAsync(Test entity);
        Task DeleteAsync(Test entity);
        Task<int> CountAsync(ISpecification<Test> spec);
    }
}