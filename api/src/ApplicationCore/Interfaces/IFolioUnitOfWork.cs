using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IFolioUnitOfWork
    {
        void SaveChanges();
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
        Task CompleteAsync();
    }
}