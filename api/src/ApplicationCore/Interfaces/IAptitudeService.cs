using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using stadpro.ApplicationCore.Interfaces;

namespace ApplicationCore.Interfaces
{
    public interface IAptitudeService
    {
        Task<Aptitude> AddAsync(Aptitude entity);
        Task<IReadOnlyList<Aptitude>> ListAllAsync();
        Task<IReadOnlyList<Aptitude>> ListAsync(ISpecification<Aptitude> spec);
        Task<Aptitude> GetByIdAsync(int id);
        Task UpdateAsync(Aptitude entity);
        Task DeleteAsync(Aptitude entity);
        Task<int> CountAsync(ISpecification<Aptitude> spec);
        void AddAptitude(Aptitude entity);
        IList<Aptitude> GetAptitudeAsync(int num, string text, bool status, DateTime start_date);
    }
}