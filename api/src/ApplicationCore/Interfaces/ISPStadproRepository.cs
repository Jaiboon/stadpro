using System;
using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces
{
    public interface ISPStadproRepository
    {
        IList<Test> GetTests(int id, string text, bool status, DateTime start_date);


    }
}