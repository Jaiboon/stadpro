using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using stadpro.ApplicationCore.Interfaces;
using System;

namespace ApplicationCore.Services
{
    public class AptitudeService : IAptitudeService
    {
        private readonly IFolioAsyscRepository<Aptitude> _folioAsyscRepository;
        private readonly ISPFolioRepository _spFolioRepository;
        private readonly IAppLogger<AptitudeService> _logger;
        private readonly IFolioUnitOfWork _unitOfWork;

        public AptitudeService(IFolioAsyscRepository<Aptitude> folioAsyscRepository,
            ISPFolioRepository spFolioRepository,
            IAppLogger<AptitudeService> logger,
            IFolioUnitOfWork unitOfWork)
        {
            _folioAsyscRepository = folioAsyscRepository;
            _spFolioRepository = spFolioRepository;
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<Aptitude> AddAsync(Aptitude entity)
        {
            Aptitude aptitude;
            try
            {
                _unitOfWork.BeginTransaction();
                aptitude = await _folioAsyscRepository.AddAsync(entity);
                _logger.LogInformation("Save success fully", aptitude.Id);
                _unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                _logger.LogWarning("Can not save", ex.Message);
                _unitOfWork.RollbackTransaction();
                throw;
            }

            return aptitude;
        }

        public Task<int> CountAsync(ISpecification<Aptitude> spec)
        {
            throw new System.NotImplementedException();
        }

        public async Task DeleteAsync(Aptitude entity)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                await _folioAsyscRepository.DeleteAsync(entity);
                _logger.LogInformation("Delete success fully");
                _unitOfWork.CommitTransaction();

            }
            catch (Exception ex)
            {
                _logger.LogWarning("Can not delete", ex.Message);
                _unitOfWork.RollbackTransaction();

            }
        }

        public async Task<Aptitude> GetByIdAsync(int id)
        {
            
            return await _folioAsyscRepository.GetByIdAsync(id);
        }

        public async Task<IReadOnlyList<Aptitude>> ListAllAsync()
        {
            return await _folioAsyscRepository.ListAllAsync();
        }

        public async Task<IReadOnlyList<Aptitude>> ListAsync(ISpecification<Aptitude> spec)
        {
            return await _folioAsyscRepository.ListAsync(spec);
        }

        public async Task UpdateAsync(Aptitude entity)
        {
            await _folioAsyscRepository.UpdateAsync(entity);
        }

        public void AddAptitude(Aptitude entity)
        {
            try
            {
                _unitOfWork.BeginTransaction();
                _spFolioRepository.SaveAptitude(entity);
                _logger.LogInformation("Save success fully");
                _unitOfWork.CommitTransaction();
            }
            catch (Exception ex)
            {
                _logger.LogWarning("Can not save", ex.Message);
                _unitOfWork.RollbackTransaction();
                throw;
            }

        }
        public IList<Aptitude> GetAptitudeAsync(int num, string text, bool status, DateTime start_date)
        {
            var resource = _spFolioRepository.GetAptitudeAsync(56, "ACBDEFG", true, DateTime.Now);
            return resource;
        }
    }
}