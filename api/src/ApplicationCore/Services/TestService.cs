using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using stadpro.ApplicationCore.Interfaces;

namespace ApplicationCore.Services
{
    public class TestService : ITestService
    {
        private readonly IAsyncRepository<Test> _asyncRepository;

        public TestService(IAsyncRepository<Test> asyncRepository)
        {
            _asyncRepository = asyncRepository;
        }

        public async Task<Test> AddAsync(Test entity)
        {
            return await _asyncRepository.AddAsync(entity);
        }

        public Task<int> CountAsync(ISpecification<Test> spec)
        {
            throw new System.NotImplementedException();
        }

        public async Task DeleteAsync(Test entity)
        {
            await _asyncRepository.DeleteAsync(entity);
        }

        public async Task<Test> GetByIdAsync(int id)
        {
            return await _asyncRepository.GetByIdAsync(id);
        }

        public async Task<IReadOnlyList<Test>> ListAllAsync()
        {
            return await _asyncRepository.ListAllAsync();
        }

        public async Task<IReadOnlyList<Test>> ListAsync(ISpecification<Test> spec)
        {
            return await _asyncRepository.ListAsync(spec);
        }

        public async Task UpdateAsync(Test entity)
        {
            await _asyncRepository.UpdateAsync(entity);
        }
    }
}