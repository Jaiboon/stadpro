﻿using System.Collections.Generic;

namespace stadpro.ApplicationCore.Entities
{
    public class CatalogBrand : BaseEntity
    {
        public string Brand { get; set; }
    }
}
