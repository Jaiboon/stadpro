﻿using System;

namespace stadpro.ApplicationCore.Entities
{
    // This can easily be modified to be BaseEntity<T> and public T Id to support different key types.
    // Using non-generic integer types for simplicity and to ease caching logic
    public class BaseEntity
    {
        public int Id { get; set; }
        public int CreateUser { get; set; }
        public DateTime CreateDate { get; set; }
        public int ModifyUser { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool CanCancle { get; set; }
        public string StatusFlag { get; set; }
    }
}
