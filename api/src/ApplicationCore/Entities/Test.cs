using stadpro.ApplicationCore.Entities;

namespace ApplicationCore.Entities
{
    public class Test : BaseEntity
    {
        public string Description { get; set; }
    }
}