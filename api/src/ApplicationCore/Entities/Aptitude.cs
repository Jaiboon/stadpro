using stadpro.ApplicationCore.Entities;

namespace ApplicationCore.Entities
{
    public class Aptitude : BaseEntity
    {
        public string Description { get; set; }
    }
}