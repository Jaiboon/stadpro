using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using stadpro.ApplicationCore.Entities;
using stadpro.Infrastructure.Data;

namespace Infrastructure.Data
{
    public class SPStadproRepository : EfRepository<BaseEntity>, ISPStadproRepository
    {
        public IConfiguration Configuration { get; }
        private int TimeOut = 300;

        public SPStadproRepository(StadproContext dbContext, IConfiguration configuration) : base(dbContext)
        {
            Configuration = configuration;
        }

        public IList<Test> GetTests(int id, string text, bool status, DateTime start_date)
        {
            IDbConnection dbConnection = new SqlConnection(Configuration.GetConnectionString("StadproConnection"));
            return Dapper.SqlMapper.Query<Test>(dbConnection, "GetAptitudes @Description",
                new { Description = text }, commandTimeout: TimeOut).ToList();
        }

        public void SaveTest(Test model)
        {
            SqlParameter[] arParams = new SqlParameter[1];
            arParams[0] = new SqlParameter("@XMLAssessmentData", model);
            _dbContext.Database.ExecuteSqlCommand("stp_save_assessment_data_xml @XMLAssessmentData", arParams);
        }
    }
}