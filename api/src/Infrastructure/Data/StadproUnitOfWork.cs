using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using stadpro.Infrastructure.Data;

namespace Infrastructure.Data
{
    public class StadproUnitOfWork : IStadproUnitOfWork
    {
        private readonly StadproContext _dbContext;

        public StadproUnitOfWork(StadproContext DbContext)
        {
            _dbContext = DbContext;
        }
        public void BeginTransaction()
        {
            _dbContext.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            _dbContext.Database.CommitTransaction();
        }

        public async Task CompleteAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public void RollbackTransaction()
        {
            _dbContext.Database.RollbackTransaction();
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}