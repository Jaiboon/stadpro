using System.Threading.Tasks;
using ApplicationCore.Interfaces;

namespace Infrastructure.Data
{
    public class FolioUnitOfWork : IFolioUnitOfWork
    {
        private readonly FolioContext _dbContext;

        public FolioUnitOfWork(FolioContext DbContext)
        {
            _dbContext = DbContext;
        }
        public void BeginTransaction()
        {
            _dbContext.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            _dbContext.Database.CommitTransaction();
        }

        public async Task CompleteAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public void RollbackTransaction()
        {
            _dbContext.Database.RollbackTransaction();
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}