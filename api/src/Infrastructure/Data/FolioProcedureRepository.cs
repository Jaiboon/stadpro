using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using stadpro.ApplicationCore.Entities;
using stadpro.Infrastructure.Data;

namespace Infrastructure.Data
{
    public class FolioProcedureRepository : FolioRepository<BaseEntity>, ISPFolioRepository
    {

        public FolioProcedureRepository(FolioContext dbContext, IConfiguration configuration) : base(dbContext)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IList<Aptitude> GetAptitudeAsync(int num, string text, bool status, DateTime start_date)
        {
            IDbConnection dbConnection = new SqlConnection(Configuration.GetConnectionString("FolioConnection"));
            var resource = Dapper.SqlMapper.Query<Aptitude>(dbConnection, "GetAptitudes @Description",
                new { Description = text }, commandTimeout: 300).ToList();

            return resource;

        }

        public void SaveAptitude(Aptitude model)
        {
            SqlParameter[] arParams = new SqlParameter[1];
            arParams[0] = new SqlParameter("@XMLAssessmentData", model);
            _dbContext.Database.ExecuteSqlCommand("stp_save_assessment_data_xml @XMLAssessmentData", arParams);
        }

        public void DeleteAptitude(Aptitude model)
        {
            SqlParameter[] arParams = new SqlParameter[1];
            arParams[0] = new SqlParameter("@AssessmentId", model.Id);

            _dbContext.Database.ExecuteSqlCommand("stp_delete_assessment_data @AssessmentId", arParams);
        }
    }
}