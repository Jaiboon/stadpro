using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class FolioContext : DbContext
    {
        public FolioContext(DbContextOptions<FolioContext> options) : base(options)
        {
        }

        public DbSet<Aptitude> Aptitudes { get; set; }


    }
}