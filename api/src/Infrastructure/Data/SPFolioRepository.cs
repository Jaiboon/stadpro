using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using stadpro.ApplicationCore.Entities;
using stadpro.Infrastructure.Data;

namespace Infrastructure.Data
{
    public class SPFolioRepository : FolioRepository<BaseEntity>, ISPFolioRepository
    {
        public IConfiguration Configuration { get; }
        private int TimeOut = 300;

        public SPFolioRepository(FolioContext dbContext, IConfiguration configuration) : base(dbContext)
        {
            Configuration = configuration;
        }

        public IList<Aptitude> GetAptitudeAsync(int num, string text, bool status, DateTime start_date)
        {
            IDbConnection dbConnection = new SqlConnection(Configuration.GetConnectionString("FolioConnection"));
            return Dapper.SqlMapper.Query<Aptitude>(dbConnection, "GetAptitudes @Description",
                new { Description = text }, commandTimeout: TimeOut).ToList();

        }

        public void SaveAptitude(Aptitude model)
        {
            SqlParameter[] arParams = new SqlParameter[3];
            arParams[0] = new SqlParameter("@Description", model.Description);
            arParams[1] = new SqlParameter("@CreateUser", model.CreateUser);
            arParams[2] = new SqlParameter("@ModifyUser", model.ModifyUser);
            _dbContext.Database.ExecuteSqlCommand("sp_aptitude_save @Description, @CreateUser, @ModifyUser", arParams);
        }

        public void DeleteAptitude(Aptitude model)
        {
            SqlParameter[] arParams = new SqlParameter[1];
            arParams[0] = new SqlParameter("@AssessmentId", model.Id);
            _dbContext.Database.ExecuteSqlCommand("stp_delete_assessment_data @AssessmentId", arParams);
        }
    }
}