/****** Object:  StoredProcedure [dbo].[GetAptitudes]    Script Date: 4/24/2019 3:54:58 PM ******/
DROP PROCEDURE [dbo].[GetAptitudes]
GO
/****** Object:  StoredProcedure [dbo].[GetAptitudes]    Script Date: 4/24/2019 3:54:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAptitudes]
	-- Add the parameters for the stored procedure here
	@Description varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select CreateUser,	CreateDate,	ModifyUser,	ModifyDate,	Description,	StatusFlag,	CanCancle
	 from Aptitudes
END

GO
